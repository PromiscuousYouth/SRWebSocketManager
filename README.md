# SRWebSocketManager
基于[SocketRocket](https://github.com/facebook/SocketRocket)封装的单例，可断开重连，定时发送心跳包，可设置重连次数，重连时间间隔。

[![Fork me on Gitee](https://gitee.com/PromiscuousYouth/SRWebSocketManager/widgets/widget_2.svg?color=3c3c55)](https://gitee.com/PromiscuousYouth/SRWebSocketManager)

#### Usage - 使用方法
```objective-c
//单例创建
[LZWebSocketManager lz_connectSocketWithUrl:@"ws://echo.websocket.org" delegate:self];

//手动创建
//不要创建成为局部变量，否则会立刻释放
self.webSocketManager = [[WebSocketManager alloc]init];
self.webSocketManager.delegate = self;
self.webSocketManager.urlString = @"ws://echo.websocket.org";
[self.websocket lz_connect];
```

#### SendMessage - 发送消息
```objective-c
[LZWebSocketManager lz_sendMessage:@"这是一条消息"];

[self.webSocketManager lz_sendMessage:@"这是一条消息"];
```

#### Delegate - 代理方法
```objective-c
#pragma mark - WebSocketManagerDelegate
/**
 已经连接
 
 @param socket WebSocketManager
 */
- (void)lz_webSocketDidConnect:(WebSocketManager *)socket {
    NSLog(@"连接成功");
    //TODO
    //可以发送登录/请求信息。。。
}

/**
 已经断开连接
 
 @param socket WebSocketManager
 */
- (void)lz_webSocketDidDisconnect:(WebSocketManager *)socket {
    NSLog(@"已经断开连接");
}

/**
 连接失败
 
 @param socket WebSocketManager
 */
- (void)lz_webSocketConnectError:(WebSocketManager *)socket {
    NSLog(@"失败");
    //TODO
}

/**
 已经收到消息
 
 @param socket WebSocketManager
 @param data 数据
 */
- (void)lz_webSocket:(WebSocketManager *)socket didReceiveMessage:(id)data {
    NSLog(@"%@",data);
    //TODO
}

/**
 消息发送失败
 
 @param socket WebSocketManager
 @param socketStatus Socket的连接状态
 */
- (void)lz_webSocket:(WebSocketManager *)socket sendMessageError:(LZWebSocketStatus)socketStatus {
    if (!socket) return;
    switch (socketStatus) {
        case LZWebSocketStatusNotConnected:
        {
            [self alertWithReconnect];

            //如果WebSocketManager被释放、此处应该手动连接
//            [socket lz_connect:@"ws://echo.websocket.org"];
            //如果没有释放，可以继续使用重连方法
//            [socket lz_reconnect];
            //或者直接连接的方法
//            [socket lz_connect];
        }
            break;
        case LZWebSocketStatusFailed:
        {
            NSLog(@"发送失败\n正在与服务器建立连接...");
        }
            break;
        case LZWebSocketStatusClosedByServer:
        case LZWebSocketStatusClosedByUser:
        {
            [self alertWithReconnect];
        }
            break;
        default:
            break;
    }
}

//手动重连提示
- (void)alertWithReconnect {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"互动聊天已与服务器断开连接\n是否尝试重新连接？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *connectAction = [UIAlertAction actionWithTitle:@"重新连接" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [LZWebSocketManager lz_reconnect];
        //[self.webSocketManager lz_reconnect];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [LZWebSocketManager lz_disconnect];
        //[self.webSocketManager lz_disconnect];
    }];
    [alert addAction:connectAction];
    [alert addAction:cancelAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alert animated:YES completion:nil];
    });
}
```